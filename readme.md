# README #

This website is based on [Hugo CMS](https://gohugo.io/) and hosted on AWS S3 bucket. 

## Run on localhost ##
Need [Hugo](https://gohugo.io/overview/installing/) to be installed locally
Run your website using the command > hugo server -t mdl_theme
Use the parameter -D to compile draft posts.

## Templating ##
The template is based on Google Material Design

## Deployment ##
Git repository is connected to a Bitbucket Pipelines. 
This allows to deploy automatically the website on the S3 Bucket when push on *master* branch.