+++
title       = "A propros - Est-ce que c'est bientôt"
description = ""
keywords    = "bientot,histoire"
url         = "a-propos"
color       = "wet-asphalt"
+++

# A propos

Nous utilisons le système CoinHive comme solution de financement. Ce système utilise 30% de votre processeur pendant votre visite afin de résoudre des calculs blokchain "Monero".
Pour plus d'informations, n'hésitez pas à nous contacter.