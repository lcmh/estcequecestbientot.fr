+++
title       = "Mentions légales - Est-ce que c'est bientôt"
description = ""
keywords    = "bientot,mentions,legale"
url         = "mentions-legales"
color       = "wet-asphalt"
+++

## GENERALITES

L'accès ou l’utilisation du Site implique l’acceptation sans réserve des présentes Conditions d’Utilisation. 

Nous nous réservons le droit de modifier les présentes Conditions d’utilisation, à tout moment et sans préavis. Les modifications prendront effet dès leur publication en ligne. Les utilisateurs sont donc invités à consulter régulièrement ces conditions pour prendre connaissance des éventuelles modifications qui y sont apportées.

## INFORMATIONS LEGALES

### Editeur

LCMH, une société à responsabilité limitée de droit français au capital de 3000,00 €, immatriculée au registre du commerce et des sociétés de Mulhouse sous le numéro 512 161 324, et ayant son siège social 6, rue de Rougemont 68200 Mulhouse, France (« LCMH »). 

Directeur de la publication : Charles Rapp

### Hébergeur

Amazon Web Service (Data Services Ireland Limited), One Kilmainham Square Inchicore Road Kilmainham, Dublin 8

### Contact
Pour toute information liée au fonctionnement ou à l’utilisation de ce site, vous pouvez écrire à contact@estcequecestbientot.fr

## LIENS VERS D’AUTRES SITES
Le Site peut renvoyer les utilisateurs vers d’autres sites de tiers et/ou de filiales ou partenaires de LCMH, par le biais de liens hypertextes notamment profonds ; ceci constitue un service mis à la disposition de l’utilisateur. LCMH décline toute responsabilité quant aux contenus de ces autres sites sur lesquels elle n’exerce aucun contrôle. Il est en conséquence de votre responsabilité de prendre connaissance avec attention de leurs conditions générales d'utilisation ou encore de leurs politiques de confidentialité.