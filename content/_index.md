+++
title       = "Est-ce que c'est bientôt"
description = "Est-ce que c'est bientôt... Retrouvez tous les est-ce que c'est bientôt sur une unique et même page."
keywords    = "estcequecestbientot"
site_name    = "Est-ce que c'est bientôt ?"
thumbnail   = ""
+++

## Est ce que c'est bientôt

L'idée originale et originelle revient à [@ternel](https://twitter.com/ternel) qui a pour la première fois lancé la mécanique est ce que c'est bientôt.
C'est donc d'abord par le week-end que tout a commencé. 
Puis les déclinaisons sont apparues avec plus ou moins de succès, certaines n'étant qu'éphémères et ne souhaitaient que surfer sur la vague "est ce que c'est bientôt"

À l'aube de l'été, avec mon collègue de l'époque, Tristan, nous décidions d'entamer un travail de listing de l'ensemble des est ce que c'est bientôt, et surtout de rendre ce travail accessible au plus grand nombre d'entre vous.

Nous sommes toujours à la recherche de nouvelles variantes, existantes ou non.

En vous remerciant pour votre contribution !