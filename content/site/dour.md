+++
title       = "Est-ce que c'est bientôt Dour ?"
short_title = "Dour"
description = ""
keywords    = "bientot,concert,dour"
url        = "dour"
site_url    = "http://estcequecestbientotdour.be/"
site_name    = "Est-ce que c'est bientôt Dour ?"
color       = "alizarin"
color2      = "pomegranate"
thumbnail   = ""
weight      = 8
+++

Créé en 1989 par Carlo Di Antonio, le Festival de Dour est aujourd’hui le plus grand événement musical et touristique de Belgique francophone. Le festival arrive depuis 28 ans à trouver l’alchimie parfaite pour créer une des plus importantes fêtes en Europe : une programmation singulière, alternative et puissante alliant artistes mythiques, découvertes et diversité, un terrain de jeu au cœur des Terrils du Borinage et une organisation sans faille, pour près de 230 000 jeunes amoureux de la musique.

En 2016, le festival a accueilli pour la première fois près de 235 000 festivaliers venus de toute l’Europe, pour 5 jours d’amour et de musique. La prochaine édition aura lieu du 12 au 16 juillet 2017 et promet d’ores et déjà de tenir toutes ses promesses : un site sans cesse repensé, un camping gigantesque mais confortable et un accueil sans cesse amélioré.

## Histoire
Dour est une commune francophone de Belgique, située en région wallonne dans la province du Hainaut. Le Dour festival voit le jour en 1989. En 2010, le festival de Dour fut élu "Meilleur festival d'Europe" , face à ses concurrents d'Espagne, de Pologne, d'Allemagne, de Suisse et bien d'autres. En 2015, le festival atteint un record de fréquentation avec 228 000 festivaliers en cumulé sur les 5 jours. En 2016, le festival atteint un nouveau record avec 235 000 participants. Le festival de Dour est aujourd’hui le plus grand événement musical et touristique de Belgique francophone. 

## Fréquentation 
Le festival prend de plus en plus d'ampleur et compte également de plus en plus de festivaliers venant de tous les pays du monde (Australie, Amérique, France, Angleterre...). Le festival affiche complet pour la première fois en 2005 avec une fréquentation de 128 000 personnes sur les quatre jours. En 2006, 134 000 personnes s'y rendent. L’événement affiche complet en 2007 plusieurs jours avant son ouverture : 144 000 festivaliers sur la durée du festival5. Le festival a dû refuser 15 000 personnes par jour6. Parmi ces festivaliers, 32 000 ont dormi au camping avec un record d'affluence de 23 000 personnes arrivées la veille de l'ouverture.

En 2009, la 21e édition du festival compte 141 000 festivaliers.

En 2013, on comptera 183 000 visiteurs, nouveau record de fréquentation.

Le festival bat à nouveau son record lors de l'édition 2014 qui a ouvert ses portes à près de 195 000 festivaliers et 38 000 campeurs en 5 jours.

Lors de l'édition 2015, le festival se déroule sur 5 jours avec une soirée spéciale le mercredi, à l'occasion de Mons 2015 et cette édition bat son record de fréquentation avec 228 000 festivaliers et 270 artistes et groupes musicaux qui ont monté sur les scènes9.

Il est déjà confirmé que la 28e édition de ce festival en 2016 se fasse également sur 5 jours.

Dour est le leader des festivals en Communauté française de Belgique en termes de visiteurs.