+++
title       = "Est-ce que c'est bientôt la retraite ?"
short_title = "la retraite"
description = "Car on l'attend tous ! "
keywords    = "retraite,estcequecestbientot"
url        = "laretraite"
site_url    = "http://estcequecestbientotlaretraite.fr/"
site_name    = "Est-ce que c'est bientôt la retraite ?"
color       = "amethyst"
color2      = "wisteria"
weight      = 3
thumbnail   = ""
+++
