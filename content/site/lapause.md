+++
title       = "Est-ce que c'est bientôt la pause ?"
short_title = "la pause"
description = "Tic tac! Est-ce bien l'heure de la pause ?"
keywords    = "bientot,pause,travail"
url        = "lapause"
site_url    = "http://estcequecestbientotlapause.fr/"
site_name    = "Est-ce que c'est bientôt la pause ?"
color       = "amethyst"
color2      = "wisteria"
thumbnail   = ""
weight      = 7
+++