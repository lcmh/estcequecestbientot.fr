+++
title       = "Est-ce que c'est bientôt la fin de la pluie ?"
short_title = "la fin de la pluie"
description = "Mieux que Météo France, la réponse en direct!"
keywords    = "pluie,inondation,estcequecestbientot"
url        = "lafindelapluie"
site_url    = "http://estcequecestbientotlafindelapluie.fr/"
site_name    = "Est-ce que c'est bientôt la fin de la pluie ?"
color       = "emerald"
color2      = "nephritis"
weight      = 3
thumbnail   = ""
draft       = "true"
+++

**Est ce que c'est bientôt la pluie** a été initié par quelques personnes de l'agence Digitas Lbi parisienne à la suite d'une longue série de jours de pluie pendant l'année 2016.

Il nous fallait absolument un outil numérique simple pour connaître la fin de cette série noire.
