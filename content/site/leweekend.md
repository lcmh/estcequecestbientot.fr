+++
title       = "Est-ce que c'est bientôt le weekend ?"
short_title = "le weekend"
description = "La question que l'on se pose tous! Mais va-t-on finir par arriver à ce p*** de weekend ?"
keywords    = "weekend,bientot,week-end,estcequecestbientotleweekend"
url        = "leweekend"
site_name    = "Est-ce que c'est bientôt le week-end ?"
color       = "amethyst"
color2      = "wisteria"
site_url    = "http://estcequecestbientotleweekend.fr/"
weight      = 1
thumbnail   = ""
draft       = "true"
+++

Lorie nous chantait :

Du Lundi au Vendredi,
On attend qu'une chose
C'est compter les jours
Tous à tour de rôle
3, 2, 1 **Ca y est c'est le week-end**

Telle est notre devise 5 jours sur 7 et plus encore à l'approche de ce fameux week-end.
Chaque vendredi, nous consultons inlassablement cette page et espérons y voir cette phrase de délivrance : **c'est le weekend ! **

C'est avec ce **"est ce que c'est bientôt"** que tout a commencé. En 2009 plus exactement, lorsque le domaine estcequecestbuentitleweekend.com a été pour la première fois enregistré. Tu es sur l'original et l'indémodable "est ce que c'est bientôt le week-end?"

En effet celui ci n'a pas pris une ride depuis le début de sa vie.
