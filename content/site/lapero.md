+++
title       = "Est-ce que c'est bientôt l'apéro ?"
short_title = "l'apéro"
description = "Midi Ricard ? A l'apéroooo!"
keywords    = "apero,bientot,heure,ricard"
url         = "lapero"
site_url    = "http://estcequecestbientotlapero.fr/"
site_name   = "Est-ce que c'est bientôt l'apéro ?"
color       = "carrot"
color2      = "pumpkin"
weight      = 4
thumbnail   = ""
+++

Activité plébiscitée et appréciée par un peu près l'ensemble des français (en tout cas si tu es là, ce n'est pas anodin), **l'apéro** est le moment de convivialité. 
Réalisable finalement à quasiment n'importe quelle heure de la journée, cette page est donc essentielle!
